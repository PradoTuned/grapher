﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Caliburn.Micro;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;

namespace Grapher.Models {
	public class Charting2DModel :PropertyChangedBase {
		private string _nameOfChart;

		public string NameOfChart {
			get { return _nameOfChart; }
			set { 
				_nameOfChart = value;
				NotifyOfPropertyChange(() => NameOfChart);
			}
		}

		private SeriesCollection _seriesList;
		public SeriesCollection SeriesList {
			get { return _seriesList; }
			set {
				_seriesList = value;
				NotifyOfPropertyChange(() => SeriesList);
			}
		}

		private BindableCollection<string> _xAxisLabels;
		public BindableCollection<string> XAxisLabels {
			get { return _xAxisLabels; }
			set {
				_xAxisLabels = value;
				NotifyOfPropertyChange(() => XAxisLabels);
			}
		}

		private Func<double, string> _yFormatter;
		public Func<double, string> YFormatter {
			get { return _yFormatter; }
			set {
				_yFormatter = value;
				NotifyOfPropertyChange(() => YFormatter);
			}
		}

		public Charting2DModel() {
			InitializeCurrentChart();
		}

		public void InitializeCurrentChart() {
			//GenerateLineSeriesForCollection();

			XAxisLabels = new BindableCollection<string> { "1000", "2000", "3000", "4000", "5000" };
			YFormatter = value => value.ToString();

			SeriesList = new SeriesCollection { GenerateLineSeriesForCollection() };
			
		}

		public void AddToSeries(LineSeries seriesToAdd) {
			SeriesList.Add(seriesToAdd);
		}

		private static readonly Random random = new Random();
		private static readonly object syncLock = new object();
		public LineSeries GenerateLineSeriesForCollection() {
			
			int randomMax = 50;
			ChartValues<ObservableValue> CollectionOfDataPoints;
			lock(syncLock) {
				CollectionOfDataPoints = new ChartValues<ObservableValue> {
				new ObservableValue(random.Next(randomMax)),
				new ObservableValue(random.Next(randomMax)),
				new ObservableValue(random.Next(randomMax)),
				new ObservableValue(random.Next(randomMax)),
				new ObservableValue(random.Next(randomMax)),
			};
			}

			NameOfChart = random.Next(randomMax).ToString();

			return  new LineSeries {
				Values = CollectionOfDataPoints,
				StrokeThickness = 4,
				Fill = Brushes.Transparent,
				PointGeometrySize = 20,
				DataLabels = false
			};
		}
	}
}