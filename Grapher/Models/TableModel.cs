﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grapher.Models {
	public class TableModel : PropertyChangedBase{
		private BindableCollection<string> _columnHeaders;
		public BindableCollection<string> ColumnHeaders {
			get { return _columnHeaders; }
			set {
				_columnHeaders = value;
				NotifyOfPropertyChange(() => ColumnHeaders);
			}
		}

		private BindableCollection<string> _rowHeaders;
		public BindableCollection<string> RowHeaders {
			get { return _rowHeaders; }
			set { 
				_rowHeaders = value;
				NotifyOfPropertyChange(() => RowHeaders);
			}
		}

		private int[,] _tableOfValues;
		public int[,] TableOfValues {
			get { return _tableOfValues; }
			set {
				_tableOfValues = value;
				NotifyOfPropertyChange(() => TableOfValues);
			}
		}

		public TableModel() { 
			ColumnHeaders = new BindableCollection<string> { "11", "22", "33" };
			RowHeaders = new BindableCollection<string> { "1", "2", "3" };

			TableOfValues = new int[3, 3];
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					TableOfValues[i, j] = i + j;
				}
			}
		}
	}
}
