﻿using System.Windows;
using Caliburn.Micro;
using Grapher.ViewModels;

namespace Grapher {
	class AppBootstrapper : BootstrapperBase {
		public AppBootstrapper() {
			Initialize();
		}

		protected override void OnStartup(object sender, StartupEventArgs e) {
			DisplayRootViewFor<GrapherViewModel>();
		}
	}
}
