﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace Grapher.ViewModels {
	public class GrapherViewModel :Conductor<object> {
		private static GrapherViewModel _instance ;
		public static GrapherViewModel Instance { get { return _instance; } }

		private BindableCollection<Charting2DViewModel> _listOfcharting2DViewModels;
		public BindableCollection<Charting2DViewModel> ListOfCharting2DViewModels {
			get { return _listOfcharting2DViewModels; }
			set { 
				_listOfcharting2DViewModels = value;
				NotifyOfPropertyChange(() => ListOfCharting2DViewModels);
			}
		}

		private Charting2DViewModel current2DViewModel;
		public Charting2DViewModel Current2DViewModel {
			get { return current2DViewModel; }
			set {
				current2DViewModel = value;
				NotifyOfPropertyChange(() => Current2DViewModel);
				Show2DChart();
			}
		}

		private TableViewModel _currentTableViewModel;
		public TableViewModel CurrentTableViewModel {
			get { return _currentTableViewModel; }
			set {
				_currentTableViewModel = value;
				NotifyOfPropertyChange(() => CurrentTableViewModel);
			}
		}

		public GrapherViewModel() {
			ListOfCharting2DViewModels = new BindableCollection<Charting2DViewModel>();
			InitializeListOFChartingViewModels();

			//Current2DViewModel = ListOfCharting2DViewModels.First();
			CurrentTableViewModel = new TableViewModel();
		}

		private void InitializeListOFChartingViewModels() {
			for(int i = 0; i < 5; i++)
				ListOfCharting2DViewModels.Add(new Charting2DViewModel());

			Current2DViewModel = ListOfCharting2DViewModels.First();
		}

		public async void Show2DChart() {
			await ActivateItemAsync(Current2DViewModel, new CancellationToken());
		}

		public void AddSeries() {
			Current2DViewModel.CurrentChartModel.AddToSeries(Current2DViewModel.CurrentChartModel.GenerateLineSeriesForCollection());
		}
	}
}
