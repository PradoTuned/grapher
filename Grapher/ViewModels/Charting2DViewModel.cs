﻿using System.Windows.Controls;
using System.Windows.Media;
using System.Threading.Tasks;
using LiveCharts;
using LiveCharts.Wpf;
using LiveCharts.Defaults;
using System;
using Caliburn.Micro;
using Grapher.Models;

namespace Grapher.ViewModels {
	public class Charting2DViewModel : Conductor<object>  {
		private Charting2DModel _currentChartModel;
		public Charting2DModel CurrentChartModel {
			get { return _currentChartModel; }
			set { 
				_currentChartModel = value;
				NotifyOfPropertyChange(() => CurrentChartModel);
			}
		}

		public Charting2DViewModel() {
			CurrentChartModel = new Charting2DModel();
		}

		private void MyChart_DataClick(object sender, ChartPoint chartPoint) {
			//TODO: Perform a click and drag even handler
			// perhaps in this function we should set a bool for the ChartPOint object to determine if it was clicked on.
		}
	}
}
