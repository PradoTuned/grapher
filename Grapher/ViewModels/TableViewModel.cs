﻿using Caliburn.Micro;
using Grapher.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grapher.ViewModels {
	public class TableViewModel : Conductor<object> {
		private TableModel _currentTableModel;
		public TableModel CurrentTableModel {
			get { return _currentTableModel; }
			set { 
				_currentTableModel = value;
				NotifyOfPropertyChange(() => CurrentTableModel);
			}
		}

		public TableViewModel() {
			CurrentTableModel = new TableModel();
		}
	}
}
